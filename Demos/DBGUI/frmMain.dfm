object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Mustache rendering demo - Using Dataset'
  ClientHeight = 410
  ClientWidth = 589
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object splVertical: TSplitter
    Left = 0
    Top = 217
    Width = 589
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 0
    ExplicitWidth = 192
  end
  object splHorizontal: TSplitter
    Left = 265
    Top = 0
    Height = 217
    ExplicitLeft = 280
    ExplicitTop = 24
    ExplicitHeight = 100
  end
  object pnlResult: TPanel
    Left = 0
    Top = 220
    Width = 589
    Height = 190
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 198
    ExplicitWidth = 546
    DesignSize = (
      589
      190)
    object lblResult: TLabel
      Left = 20
      Top = 15
      Width = 30
      Height = 13
      Caption = 'Result'
    end
    object btnRender: TButton
      Left = 225
      Top = 10
      Width = 123
      Height = 25
      Anchors = [akTop]
      Caption = 'Render Template'
      TabOrder = 0
      OnClick = btnRenderClick
      ExplicitLeft = 204
    end
    object cbHTML: TCheckBox
      Left = 473
      Top = 14
      Width = 97
      Height = 17
      Anchors = [akTop, akRight]
      Caption = 'Render HTML'
      Checked = True
      State = cbChecked
      TabOrder = 1
      ExplicitLeft = 430
    end
    object pcResult: TPageControl
      Left = 16
      Top = 37
      Width = 554
      Height = 140
      ActivePage = tsMemo
      Anchors = [akLeft, akTop, akRight, akBottom]
      Style = tsFlatButtons
      TabOrder = 2
      TabStop = False
      ExplicitWidth = 511
      object tsMemo: TTabSheet
        Caption = 'tsMemo'
        TabVisible = False
        ExplicitWidth = 503
        object mResult: TMemo
          Left = 0
          Top = 0
          Width = 546
          Height = 130
          Align = alClient
          Lines.Strings = (
            'mResult')
          TabOrder = 0
          ExplicitWidth = 503
        end
      end
      object tsWebBrowser: TTabSheet
        Caption = 'tsWebBrowser'
        ImageIndex = 1
        TabVisible = False
        ExplicitWidth = 503
        object wbResult: TWebBrowser
          Left = 0
          Top = 0
          Width = 546
          Height = 130
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 300
          ExplicitHeight = 150
          ControlData = {
            4C0000006E380000700D00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
  end
  object pnlTemplate: TPanel
    Left = 0
    Top = 0
    Width = 265
    Height = 217
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 195
    DesignSize = (
      265
      217)
    object lblTemplate: TLabel
      Left = 16
      Top = 8
      Width = 44
      Height = 13
      Caption = '&Template'
    end
    object mTemplate: TMemo
      Left = 16
      Top = 27
      Width = 233
      Height = 176
      Anchors = [akLeft, akTop, akRight, akBottom]
      Lines.Strings = (
        '<html>'
        '  <head>'
        '    <title>{{title}}</title>'
        '  </head>'
        '    <body>'
        '      <h1>{{title}}</h1>'
        '      <table>'
        '        <thead>'
        '          <tr>'
        '            <th>Name</th><th>Iso</th>'
        '          </tr>'
        '        </thead>'
        '        <tbody>'
        '{{#data}}'
        '        <tr>  '
        '          <td>{{name}}</td><td>{{iso}}</td>'
        '        </tr> '
        '{{/data}}'
        '        </tbody>'
        '      </table>'
        '    </body>'
        '</html>')
      TabOrder = 0
      ExplicitHeight = 154
    end
  end
  object pnlJSON: TPanel
    Left = 268
    Top = 0
    Width = 321
    Height = 217
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlJSON'
    TabOrder = 2
    ExplicitWidth = 278
    ExplicitHeight = 195
    DesignSize = (
      321
      217)
    object lblData: TLabel
      Left = 16
      Top = 8
      Width = 23
      Height = 13
      Caption = 'Data'
    end
    object DBNavigator1: TDBNavigator
      Left = 69
      Top = 10
      Width = 240
      Height = 25
      DataSource = dsCountries
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 41
      Width = 292
      Height = 162
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = dsCountries
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'iso'
          Title.Caption = 'ISO'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'name'
          Title.Caption = 'Name'
          Width = 200
          Visible = True
        end>
    end
  end
  object cdsCountries: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 316
    Top = 56
  end
  object dsCountries: TDataSource
    DataSet = cdsCountries
    Left = 356
    Top = 56
  end
end
