unit frmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Mustache, System.JSON, Vcl.OleCtrls, SHDocVw;


type
  TMainForm = class(TForm)
    pnlResult: TPanel;
    lblResult: TLabel;
    btnRender: TButton;
    splVertical: TSplitter;
    pnlTemplate: TPanel;
    mTemplate: TMemo;
    lblTemplate: TLabel;
    splHorizontal: TSplitter;
    pnlJSON: TPanel;
    lblJSON: TLabel;
    mJSON: TMemo;
    cbHTML: TCheckBox;
    pcResult: TPageControl;
    tsMemo: TTabSheet;
    tsWebBrowser: TTabSheet;
    mResult: TMemo;
    wbResult: TWebBrowser;
    procedure FormResize(Sender: TObject);
    procedure btnRenderClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FMustache : TMustache;
    Function GetJSON : TJSONValue;
    procedure ShowAsHTML(aHTML: String);
  public

    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}
uses MSHTML;

procedure TMainForm.ShowAsHTML(aHTML : String);

  // Inspiration taken from
  // https://stackoverflow.com/questions/11954946/how-to-convert-simple-richtext-to-html-tags-in-delphi

  function WaitDocumentReady: Boolean;
  var
    StartTime: DWORD;
  begin
    StartTime := GetTickCount;
    while wbResult.ReadyState <> READYSTATE_COMPLETE do
    begin
      Application.HandleMessage;
      if GetTickCount >= StartTime + 2000 then // time-out of max 2 sec
      begin
        Result := False; // time-out
        Exit;
      end;
    end;
    Result := True;
  end;

begin
  wbResult.Navigate('about:blank');
  if WaitDocumentReady then
    (wbResult.Document as IHTMLDocument2).body.innerHTML:=aHTML;
end;

procedure TMainForm.btnRenderClick(Sender: TObject);


Var
  Res : String;

begin
  Res:=fMustache.Render(mTemplate.Text,mJSON.Text);
  if cbHTML.Checked then
    begin
    pcResult.ActivePage:=tsWebBrowser;
    ShowAsHTML(Res);
    end
  else
    begin
    mResult.Text:=Res;
    pcResult.ActivePage:=tsMemo
    end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FMustache:=TMustache.Create(Self);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  pnlResult.Height:=(ClientHeight - splVertical.Height) div 2;
  pnlTemplate.Width:=(ClientWidth - splHorizontal.Width) div 2;
end;

function TMainForm.GetJSON: TJSONValue;
begin
  Result:=TJSONObject.ParseJSONValue(mJSON.Text,True,True);
end;

end.
