object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Mustache rendering demo - Using JSON'
  ClientHeight = 388
  ClientWidth = 546
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object splVertical: TSplitter
    Left = 0
    Top = 195
    Width = 546
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 0
    ExplicitWidth = 192
  end
  object splHorizontal: TSplitter
    Left = 265
    Top = 0
    Height = 195
    ExplicitLeft = 280
    ExplicitTop = 24
    ExplicitHeight = 100
  end
  object pnlResult: TPanel
    Left = 0
    Top = 198
    Width = 546
    Height = 190
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 201
    DesignSize = (
      546
      190)
    object lblResult: TLabel
      Left = 20
      Top = 15
      Width = 30
      Height = 13
      Caption = 'Result'
    end
    object btnRender: TButton
      Left = 204
      Top = 10
      Width = 123
      Height = 25
      Anchors = [akTop]
      Caption = 'Render Template'
      TabOrder = 0
      OnClick = btnRenderClick
    end
    object cbHTML: TCheckBox
      Left = 430
      Top = 14
      Width = 97
      Height = 17
      Anchors = [akTop, akRight]
      Caption = 'Render HTML'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object pcResult: TPageControl
      Left = 16
      Top = 37
      Width = 511
      Height = 140
      ActivePage = tsMemo
      Anchors = [akLeft, akTop, akRight, akBottom]
      Style = tsFlatButtons
      TabOrder = 2
      TabStop = False
      object tsMemo: TTabSheet
        Caption = 'tsMemo'
        TabVisible = False
        object mResult: TMemo
          Left = 0
          Top = 0
          Width = 502
          Height = 130
          Align = alClient
          Lines.Strings = (
            'mResult')
          TabOrder = 0
          ExplicitLeft = -4
          ExplicitTop = 3
          ExplicitWidth = 511
          ExplicitHeight = 140
        end
      end
      object tsWebBrowser: TTabSheet
        Caption = 'tsWebBrowser'
        ImageIndex = 1
        TabVisible = False
        ExplicitHeight = 172
        object wbResult: TWebBrowser
          Left = 0
          Top = 0
          Width = 503
          Height = 130
          Align = alClient
          TabOrder = 0
          ExplicitLeft = 136
          ExplicitTop = 16
          ExplicitWidth = 300
          ExplicitHeight = 150
          ControlData = {
            4C000000021F0000810F00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
  end
  object pnlTemplate: TPanel
    Left = 0
    Top = 0
    Width = 265
    Height = 195
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 189
    DesignSize = (
      265
      195)
    object lblTemplate: TLabel
      Left = 16
      Top = 8
      Width = 44
      Height = 13
      Caption = '&Template'
    end
    object mTemplate: TMemo
      Left = 16
      Top = 27
      Width = 233
      Height = 154
      Anchors = [akLeft, akTop, akRight, akBottom]
      Lines.Strings = (
        '<h1>{{header}}</h1>'
        '{{#bug}}'
        '{{/bug}}'
        ''
        '{{#items}}'
        '  {{#first}}'
        '    <li><strong>{{name}}</strong></li>'
        '  {{/first}}'
        '  {{#link}}'
        '    <li><a href="{{url}}">{{name}}'
        '</a></li>'
        '  {{/link}}'
        '{{/items}}'
        ''
        '{{#empty}}'
        '  <p>The list is empty.</p>'
        '{{/empty}}')
      TabOrder = 0
    end
  end
  object pnlJSON: TPanel
    Left = 268
    Top = 0
    Width = 278
    Height = 195
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlJSON'
    TabOrder = 2
    ExplicitLeft = 271
    ExplicitTop = -3
    ExplicitHeight = 189
    DesignSize = (
      278
      195)
    object lblJSON: TLabel
      Left = 18
      Top = 8
      Width = 164
      Height = 13
      Caption = 'JSON (try setting "empty" to true)'
    end
    object mJSON: TMemo
      Left = 18
      Top = 27
      Width = 241
      Height = 154
      Anchors = [akLeft, akTop, akRight, akBottom]
      Lines.Strings = (
        '{'
        '  "header": "Colors",'
        '  "items": ['
        '      {"name": "red", "first": true, "url": "#Red"},'
        '      {"name": "green", "link": true, "url": '
        '"#Green"},'
        '      {"name": "blue", "link": true, "url": "#Blue"}'
        '  ],'
        '  "empty": false'
        '}')
      TabOrder = 0
    end
  end
end
