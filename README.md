# MustacheD

A Delphi Mustache templates implementation.

This is a port of the Free Pascal mustache implementation. 

Features:

* Fully compliant with Mustache specs.
* Component appears on toolbar.
* Unit tested
* JSON data is standard
* Use Datasets as source of data.
* Highly configurable: creating descendents is easy and encouraged.

# Directory structure:

* Root directory: contains a project group which contains packages, test projects and demos.
* **Src**
  Contains 2 packages: mustached (runtime) and dmustached (design-time)
* **Tests**
  Contains the DUnit test project.
* **Demos**
  Contains several demo projects  
   * **GUI** a GUI demo project using JSON as the data source.
![](guidemo.png)  
   * **DBGUI** a GUI demo project using a dataset as the data source.
![](dbguidemo.png)  

# License:

The code is licensed under the MIT license, i.e. you can freely use it without restrictions.
