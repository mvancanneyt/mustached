unit fpcunithelpers;

interface

uses TestFramework;

Type
  TFPCUnitTestCaseHelper = Class helper for TTestCase
  Public
    Procedure AssertTrue(aValue : Boolean); overload; inline;
    Procedure AssertTrue(Const Msg : String; aValue : Boolean); overload; inline;
    Procedure AssertFalse(aValue : Boolean); overload; inline;
    Procedure AssertFalse(Const Msg : String; aValue : Boolean); overload; inline;
    Procedure AssertEquals(const Msg : String; const aExpected,aActual : String); overload; inline;
    Procedure AssertEquals(const aExpected,aActual : String); overload; inline;
    Procedure AssertEquals(const Msg : String; const aExpected,aActual : TClass); overload; inline;
    Procedure AssertEquals(const aExpected,aActual : TClass); overload; inline;
    Procedure AssertEquals(const Msg : String; const aExpected,aActual : Integer); overload; inline;
    Procedure AssertEquals(const aExpected,aActual : Integer); overload; inline;
    Procedure AssertEquals(const Msg : String; const aExpected,aActual : Double); overload; inline;
    Procedure AssertEquals(const aExpected,aActual : Double); overload; inline;
    Procedure AssertEquals(const Msg : String; const aExpected,aActual : Boolean); overload; inline;
    Procedure AssertEquals(const aExpected,aActual : Boolean); overload; inline;
    Procedure AssertNotNull(aValue : TObject);overload; inline;
    Procedure AssertNotNull(Const Msg : String; aValue : TObject);overload; inline;
    Procedure AssertNotNull(aValue : IInterface);overload; inline;
    Procedure AssertNotNull(Const Msg : String; aValue : IInterface);overload; inline;
    Procedure AssertNull(aValue : TObject);overload; inline;
    Procedure AssertNull(Const Msg : String; aValue : TObject);overload; inline;
    Procedure AssertNull(aValue : IInterface);overload; inline;
    Procedure AssertNull(Const Msg : String; aValue : IInterface);overload; inline;
    Procedure AssertSame(aExpected,aActual: TObject);overload; inline;
    Procedure AssertSame(Const Msg : String; aExpected,aActual : TObject);overload; inline;
    Procedure AssertException(Const Msg : String; AExceptionClass: TClass; aMethod : TTestMethod);overload; inline;
    Procedure AssertException(AExceptionClass: TClass; aMethod : TTestMethod);overload; inline;
  end;

  TTestCaseClass = Class of TTestCase;

Procedure RegisterTest(aClass : TTestCaseClass); overload;
Procedure RegisterTests(aClasses : array of TTestCaseClass); overload;

implementation

Procedure RegisterTest(aClass : TTestCaseClass);

begin
  RegisterTest(aClass.Suite);
end;

Procedure RegisterTests(aClasses : array of TTestCaseClass); overload;

Var
  T : TTestCaseClass;

begin
  for T in aClasses do
    RegisterTest(T);
end;

{ TFPCUnitTestCaseHelper }

procedure TFPCUnitTestCaseHelper.AssertEquals(const aExpected, aActual: String);
begin
  CheckEquals(aExpected,aActual);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const aExpected, aActual: TClass);
begin
  CheckEquals(aExpected,aActual);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const Msg: String; const aExpected, aActual: TClass);
begin
  CheckEquals(aExpected,aActual,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertFalse(const Msg: String; aValue: Boolean);
begin
  CheckFalse(aValue,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertFalse(aValue: Boolean);
begin
  CheckFalse(aValue);
end;

procedure TFPCUnitTestCaseHelper.AssertNotNull(const Msg: String; aValue: IInterface);
begin
  CheckNotNull(aValue,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertNull(aValue: TObject);
begin
  CheckNull(aValue);
end;

procedure TFPCUnitTestCaseHelper.AssertNull(const Msg: String; aValue: TObject);
begin
  CheckNull(aValue,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertNull(aValue: IInterface);
begin
  CheckNull(aValue);
end;

procedure TFPCUnitTestCaseHelper.AssertNull(const Msg: String; aValue: IInterface);
begin
  CheckNull(aValue,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertTrue(const Msg: String; aValue: Boolean);
begin
  CheckTrue(aValue,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertTrue(aValue: Boolean);
begin
  CheckTrue(aValue);
end;

procedure TFPCUnitTestCaseHelper.AssertNotNull(aValue: IInterface);
begin
  CheckNotNull(aValue);
end;

procedure TFPCUnitTestCaseHelper.AssertNotNull(const Msg: String; aValue: TObject);
begin
  CheckNotNull(aValue,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertNotNull(aValue: TObject);
begin
  CheckNotNull(aValue);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const Msg, aExpected, aActual: String);
begin
  CheckEquals(aExpected,aActual,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const aExpected, aActual: Integer);
begin
  CheckEquals(aExpected,AActual);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const Msg: String; const aExpected, aActual: Integer);
begin
  CheckEquals(aExpected,AActual,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const Msg: String; const aExpected, aActual: Double);
begin
  CheckEquals(aExpected,AActual,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const aExpected, aActual: Double);
begin
  CheckEquals(aExpected,AActual);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const Msg: String; const aExpected, aActual: Boolean);
begin
  CheckEquals(aExpected,AActual,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertEquals(const aExpected, aActual: Boolean);
begin
  CheckEquals(aExpected,AActual);
end;

procedure TFPCUnitTestCaseHelper.AssertException(AExceptionClass: TClass; aMethod: TTestMethod);
begin
  CheckException(aMethod,AExceptionClass);
end;

procedure TFPCUnitTestCaseHelper.AssertException(const Msg: String; AExceptionClass: TClass; aMethod: TTestMethod);
begin
  CheckException(aMethod,AExceptionClass,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertSame(const Msg: String; aExpected, aActual: TObject);
begin
  CheckSame(aExpected,AActual,Msg);
end;

procedure TFPCUnitTestCaseHelper.AssertSame(aExpected, aActual: TObject);
begin
  CheckSame(aExpected,AActual);
end;

end.
