program TestMustache;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  System.SysUtils,
  TestInsight.Client,
  TestInsight.DUnit,
  DUnitTestRunner,
  tcbasemustache in 'tcbasemustache.pas',
  tcdbmustache in 'tcdbmustache.pas',
  tcmustache in 'tcmustache.pas',
  fpcunithelpers in 'fpcunithelpers.pas',
  tcspecs in 'tcspecs.pas';

{$R *.RES}
function IsTestInsightRunning: Boolean;
var
  client: ITestInsightClient;
begin
  client := TTestInsightRestClient.Create;
  client.StartedTesting(0);
  Result := not client.HasError;
end;

begin
  if IsTestInsightRunning and not FindCmdLineSwitch('G',['-','/'],True) then
    TestInsight.DUnit.RunRegisteredTests
  else
     DUnitTestRunner.RunRegisteredTests;
end.

